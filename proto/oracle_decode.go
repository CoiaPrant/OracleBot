package proto

import "encoding/json"

func (x *Task) Decode(v any) error {
	if x == nil || len(x.Data) == 0 {
		return nil
	}

	return json.Unmarshal(x.Data, v)
}

func (x *Result) Decode(v any) error {
	if x == nil || len(x.Data) == 0 {
		return nil
	}

	return json.Unmarshal(x.Data, v)
}
