module OracleBot

go 1.22

require (
	github.com/blang/semver v3.5.1+incompatible
	github.com/c-robinson/iplib v1.0.8
	github.com/oracle/oci-go-sdk/v65 v65.60.0
	github.com/orcaman/concurrent-map v1.0.0
	gitlab.com/CoiaPrant/clog v0.0.0-20240125121733-757221e3362e
	gitlab.com/CoiaPrant/go-gitlab-selfupdate v0.0.0-20230525162113-1e95d977a072
	google.golang.org/grpc v1.62.0
	google.golang.org/protobuf v1.32.0
)

require (
	github.com/fatih/color v1.16.0 // indirect
	github.com/gofrs/flock v0.8.1 // indirect
	github.com/golang/protobuf v1.5.3 // indirect
	github.com/google/go-querystring v1.1.0 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
	github.com/hashicorp/go-retryablehttp v0.7.5 // indirect
	github.com/inconshreveable/go-update v0.0.0-20160112193335-8152e7eb6ccf // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.20 // indirect
	github.com/sony/gobreaker v0.5.0 // indirect
	github.com/ulikunitz/xz v0.5.11 // indirect
	github.com/xanzy/go-gitlab v0.98.0 // indirect
	golang.org/x/net v0.21.0 // indirect
	golang.org/x/oauth2 v0.17.0 // indirect
	golang.org/x/sys v0.17.0 // indirect
	golang.org/x/text v0.14.0 // indirect
	golang.org/x/time v0.5.0 // indirect
	google.golang.org/appengine v1.6.8 // indirect
	google.golang.org/genproto/googleapis/rpc v0.0.0-20240228224816-df926f6c8641 // indirect
)
