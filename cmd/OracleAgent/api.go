package main

import (
	"context"
	"crypto/tls"
	"errors"
	"strconv"
	"sync"
	"time"

	pb "OracleBot/proto"
	. "OracleBot/types"

	cmap "github.com/orcaman/concurrent-map"
	"gitlab.com/CoiaPrant/clog"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
)

var (
	ErrClientDown = errors.New("api client was down")

	apiClient     Client
	reporter      = make(chan *pb.Result, 128)
	receivedTasks = cmap.New()
)

type Client struct {
	client pb.OracleClient
	mu     sync.RWMutex

	ctx  context.Context
	done context.CancelFunc
}

func api_init(id int64, secret string) {
	var session string

	tlsConf := grpc.WithTransportCredentials(credentials.NewTLS(&tls.Config{}))

	for {
		apiClient.mu.Lock()
		time.Sleep(5 * time.Second)

		timeoutCtx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		conn, err := grpc.DialContext(timeoutCtx, "oracle.gov.cooking:443", tlsConf, grpc.WithPerRPCCredentials(&auth{Session: &session}))
		cancel()

		if err != nil {
			apiClient.mu.Unlock()

			clog.Errorf("[API] Connection failed, error: %v", err)
			continue
		}

		{
			apiClient.client = pb.NewOracleClient(conn)

			response, err := apiClient.client.Login(context.Background(), &pb.LoginRequest{Version: version, Id: id, Secret: secret})
			if err != nil {
				apiClient.client = nil
				apiClient.mu.Unlock()

				clog.Errorf("[API] Login failed, error: %v", err)
				continue
			}

			if response.Outdate {
				updateSelf()
				apiClient.client = nil
				apiClient.mu.Unlock()

				clog.Errorf("[API] Login failed, remote message: %v", response.Msg)
				continue
			}

			if !response.Ok {
				apiClient.client = nil
				apiClient.mu.Unlock()

				clog.Errorf("[API] Login failed, remote message: %v", response.Msg)
				continue
			}
			clog.Info("[API] Login success")

			apiClient.ctx, apiClient.done = context.WithCancel(context.Background())

			session = response.Session
		}

		go apiClient.receiveTasks()
		go apiClient.reportTasks()
		apiClient.mu.Unlock()

		<-apiClient.ctx.Done()
	}
}

func (c *Client) receiveTasks() {
	c.mu.RLock()
	defer c.mu.RUnlock()

	if apiClient.client == nil {
		return
	}

	recevier, err := apiClient.client.ReceiveTask(context.Background(), &pb.Empty{})
	if err != nil {
		c.done()
		clog.Debugf("[API] receiveTask failed, error: %v", err)
		return
	}

receiveLoop:
	for {
		select {
		case <-c.ctx.Done():
			return
		default:
			task, err := recevier.Recv()
			if err != nil {
				c.done()
				clog.Debugf("[API] receive task failed, error: %v", err)
				return
			}

			switch task.Type {
			case Task_Ping:
				continue receiveLoop
			case Task_Temporary:
				clog.Debugf("[API] received temporary task, id: %v", task.Id)
				go doTask(&Task{Task: task})
			case Task_Enduring:
				if receivedTasks.Has(strconv.FormatInt(task.Id, 10)) {
					continue receiveLoop
				}

				clog.Debugf("[API] received enduring task, id: %v", task.Id)

				t := &Task{Task: task}
				go doTask(t)
				receivedTasks.Set(strconv.FormatInt(task.Id, 10), t)
			case Task_CancelEnduring:
				t, ok := receivedTasks.Get(strconv.FormatInt(task.Id, 10))
				if !ok {
					continue receiveLoop
				}
				clog.Debugf("[API] received cancel enduring task, id: %v", task.Id)

				go t.(*Task).cancel()
			}
		}
	}
}

func (c *Client) reportTasks() {
	c.mu.RLock()
	defer c.mu.RUnlock()

	if apiClient.client == nil {
		return
	}

	for {
		select {
		case <-c.ctx.Done():
			return
		case result := <-reporter:
			_, err := apiClient.client.ReportTask(context.Background(), result)
			if err != nil {
				c.done()
				clog.Debugf("[API] report task failed, error: %v", err)

				if result.Type == Task_Enduring {
					go func() {
						reporter <- result
					}()
				}
				return
			}

			if result.Type == Task_Enduring {
				receivedTasks.Remove(strconv.FormatInt(result.Id, 10))
			}
		}
	}
}

func (c *Client) Logout() (err error) {
	c.mu.RLock()
	defer c.mu.RUnlock()

	if apiClient.client == nil {
		err = ErrClientDown
		return
	}

	_, err = apiClient.client.Logout(context.Background(), &pb.Empty{})
	if err != nil {
		clog.Debugf("[API] Logout failed, error: %v", err)
		return
	}

	clog.Debugf("[API] Logout successfully")
	return
}
