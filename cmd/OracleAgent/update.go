package main

import (
	"os"

	"github.com/blang/semver"
	selfupdate "gitlab.com/CoiaPrant/go-gitlab-selfupdate"
	"gitlab.com/CoiaPrant/clog"
)

func updateSelf() {
	clog.Info("[Update] starting progress...")

	if version == "dev" {
		clog.Error("[Update] failed to update, error: current version is dev")
		return
	}

	current, err := semver.Parse(version)
	if err != nil {
		clog.Errorf("[Update] failed to parse current version, error: %s", err)
		return
	}

	latest, err := selfupdate.UpdateSelf2(current, "CoiaPrant/OracleBot", "OracleAgent")
	if err != nil {
		clog.Errorf("[Update] failed to update, error: %s", err)
		return
	}
	clog.Successf("[Update] Get latest version: %s", latest.Name)

	if !latest.Version.Equals(current) {
		clog.Success("[Update] Completed, restarting...")
		os.Exit(1)
		return
	}

	clog.Success("[Update] No update available, Skip...")
}
