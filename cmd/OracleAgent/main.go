package main

import (
	"flag"
	"fmt"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"github.com/oracle/oci-go-sdk/v65/common"
	"github.com/oracle/oci-go-sdk/v65/core"
	"github.com/oracle/oci-go-sdk/v65/identity"
	"gitlab.com/CoiaPrant/clog"
)

var accounts = make(map[string]*account)
var version = "dev"

func main() {
	{
		var id int64
		var secret string
		flag.Int64Var(&id, "id", 0, "The user id")
		flag.StringVar(&secret, "secret", "", "The user secret")
		flag.BoolVar(clog.DebugFlag(), "debug", false, "Debug mode")

		help := flag.Bool("h", false, "Show help")
		v := flag.Bool("v", false, "Show version")
		flag.Parse()

		if *help {
			flag.PrintDefaults()
			return
		}

		if *v {
			fmt.Printf(version)
			return
		}

		if id == 0 || secret == "" {
			clog.Fatal("Invalid params, id or secret is empty.")
			return
		}

		files, err := os.ReadDir(".")
		if err != nil {
			clog.Fatalf("[Conf] failed to read dir, error: %s", err)
			return
		}

		for _, file := range files {
			if file.IsDir() {
				continue
			}

			if !strings.HasSuffix(file.Name(), ".cfg") {
				continue
			}

			provider, err := common.ConfigurationProviderFromFile(file.Name(), "")
			if err != nil {
				clog.Errorf("[Conf] failed to load %s, error: %s", file.Name(), err)
				continue
			}

			compute, err := core.NewComputeClientWithConfigurationProvider(provider)
			if err != nil {
				clog.Errorf("[Conf] failed to load %s, error: %s", file.Name(), err)
				continue
			}

			network, err := core.NewVirtualNetworkClientWithConfigurationProvider(provider)
			if err != nil {
				clog.Errorf("[Conf] failed to load %s, error: %s", file.Name(), err)
				continue
			}

			storage, err := core.NewBlockstorageClientWithConfigurationProvider(provider)
			if err != nil {
				clog.Errorf("[Conf] failed to load %s, error: %s", file.Name(), err)
				continue
			}

			identity, err := identity.NewIdentityClientWithConfigurationProvider(provider)
			if err != nil {
				clog.Errorf("[Conf] failed to load %s, error: %s", file.Name(), err)
				continue
			}

			accountName := strings.TrimSuffix(file.Name(), ".cfg")
			accounts[accountName] = &account{
				ConfigurationProvider: provider,
				compute:  compute,
				network:  network,
				storage:  storage,
				identity: identity,
			}
			clog.Infof("[Account] Loaded %s", accountName)
		}

		go api_init(id, secret)
	}

	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM)

	<-sigs
	clog.Print("Exiting\n")
}
