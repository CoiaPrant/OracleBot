package main

import (
	pb "OracleBot/proto"
	. "OracleBot/types"
	"context"
	"encoding/json"

	"gitlab.com/CoiaPrant/clog"
)

func generateResult(v any) *pb.Result {
	data, err := json.Marshal(v)
	if err != nil {
		clog.Errorf("[Task][Encode] failed to encode data, error: %s", err)

		return &pb.Result{
			Error: Error_Encode,
		}
	}

	return &pb.Result{
		Error: Error_NoError,
		Data:  data,
	}
}

func generateError(e error) *pb.Result {
	if e == context.Canceled {
		return &pb.Result{
			Error: Error_Canceled,
		}
	}

	data, err := json.Marshal(&Error{
		Error: e.Error(),
	})

	if err != nil {
		clog.Errorf("[Task][Encode] failed to encode error, error: %s", err)

		return &pb.Result{
			Error: Error_Encode,
		}
	}

	return &pb.Result{
		Error: Error_Other,
		Data:  data,
	}
}
