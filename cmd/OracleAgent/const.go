package main

import (
	"time"

	"github.com/oracle/oci-go-sdk/v65/common"
)

var (
	defaultPolicy = func() *common.RetryPolicy {
		policy := common.NewRetryPolicyWithOptions(
			// only base off DefaultRetryPolicyWithoutEventualConsistency() if we're not handling eventual consistency
			common.WithConditionalOption(true, common.ReplaceWithValuesFromRetryPolicy(common.DefaultRetryPolicyWithoutEventualConsistency())),
			common.WithMaximumNumberAttempts(3),
			common.WithShouldRetryOperation(common.DefaultShouldRetryOperation))
		return &policy
	}()

	alwaysPolicy = func() *common.RetryPolicy {
		sleep := func(r common.OCIOperationResponse) time.Duration {
			if r.Response.HTTPResponse().StatusCode == 429 {
				return 5 * time.Second
			}

			return 1 * time.Second
		}

		// retry for all non-200 status code
		retryOnAllNon200ResponseCodes := func(r common.OCIOperationResponse) bool {
			return !(r.Error == nil && 199 < r.Response.HTTPResponse().StatusCode && r.Response.HTTPResponse().StatusCode < 300)
		}

		policy := common.NewRetryPolicyWithOptions(
			// only base off DefaultRetryPolicyWithoutEventualConsistency() if we're not handling eventual consistency
			common.WithConditionalOption(true, common.ReplaceWithValuesFromRetryPolicy(common.DefaultRetryPolicyWithoutEventualConsistency())),
			common.WithMaximumNumberAttempts(10),
			common.WithNextDuration(sleep),
			common.WithShouldRetryOperation(retryOnAllNon200ResponseCodes))
		return &policy
	}()

	policy = func() *common.RetryPolicy {
		sleep := func(r common.OCIOperationResponse) time.Duration {
			if r.Response.HTTPResponse().StatusCode == 429 {
				return 5 * time.Second
			}

			return 1 * time.Second
		}

		policy := common.NewRetryPolicyWithOptions(
			// only base off DefaultRetryPolicyWithoutEventualConsistency() if we're not handling eventual consistency
			common.WithConditionalOption(true, common.ReplaceWithValuesFromRetryPolicy(common.DefaultRetryPolicyWithoutEventualConsistency())),
			common.WithMaximumNumberAttempts(0),
			common.WithNextDuration(sleep),
			common.WithShouldRetryOperation(common.DefaultShouldRetryOperation))
		return &policy
	}()
)
