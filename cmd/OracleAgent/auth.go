package main

import (
	"context"
)

type auth struct {
	Session *string
}

func (a *auth) GetRequestMetadata(ctx context.Context, uri ...string) (map[string]string, error) {
	return map[string]string{
		"session": *a.Session,
	}, nil
}

func (a *auth) RequireTransportSecurity() bool {
	return false
}
