package main

import (
	"github.com/oracle/oci-go-sdk/v65/common"
	"github.com/oracle/oci-go-sdk/v65/core"
	"github.com/oracle/oci-go-sdk/v65/identity"
)

type account struct {
	common.ConfigurationProvider
	compute  core.ComputeClient
	network  core.VirtualNetworkClient
	storage  core.BlockstorageClient
	identity identity.IdentityClient
}
