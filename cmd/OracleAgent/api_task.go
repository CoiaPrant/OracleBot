package main

import (
	pb "OracleBot/proto"
	. "OracleBot/types"
	"context"

	"github.com/c-robinson/iplib"
	"github.com/oracle/oci-go-sdk/v65/common"
	"github.com/oracle/oci-go-sdk/v65/core"
	"github.com/oracle/oci-go-sdk/v65/identity"
	"gitlab.com/CoiaPrant/clog"
)

type Task struct {
	*pb.Task
	ctx    context.Context
	cancel context.CancelFunc
}

func doTask(task *Task) {
	task.ctx, task.cancel = context.WithCancel(context.Background())
	defer task.cancel()

	var result *pb.Result
	switch task.Function {
	case Function_ListAccount:
		result = task.ListAccount()
	case Function_ListAvailabilityDomains:
		result = task.ListAvailabilityDomains()
	case Function_ListShapes:
		result = task.ListShapes()
	case Function_ListImages:
		result = task.ListImages()
	case Function_ListInstances:
		result = task.ListInstances()
	case Function_CreateInstance:
		result = task.CreateInstance()
	case Function_GetInstance:
		result = task.GetInstance()
	case Function_UpdateInstance:
		result = task.UpdateInstance()
	case Function_InstanceAction:
		result = task.InstanceAction()
	case Function_TerminateInstance:
		result = task.TerminateInstance()
	case Function_InstanceConsole:
		result = task.InstanceConsole()
	case Function_ChangeIPv4:
		result = task.ChangeIPv4()
	case Function_ChangeIPv6:
		result = task.ChangeIPv6()
	case Function_ListBootVolumes:
		result = task.ListBootVolumes()
	default:
		result = task.Unknown()
	}

	result.Type = task.Type
	result.Id = task.Id
	reporter <- result
}

func (t *Task) ListAccount() *pb.Result {
	var names []string
	for name := range accounts {
		names = append(names, name)
	}

	return generateResult(names)
}

func (t *Task) ListAvailabilityDomains() *pb.Result {
	account, ok := accounts[t.Account]
	if !ok {
		return &pb.Result{
			Error: Error_AccountNotFound,
		}
	}

	tenancy, err := account.TenancyOCID()
	if err != nil {
		clog.Errorf("[Task][List Availability Domains] failed to get tenancy, account %s, error: %s", t.Account, err)
		return generateError(err)
	}

	response, err := account.identity.ListAvailabilityDomains(t.ctx,
		identity.ListAvailabilityDomainsRequest{
			CompartmentId: &tenancy,
			RequestMetadata: common.RequestMetadata{
				RetryPolicy: defaultPolicy,
			},
		})
	if err != nil {
		clog.Errorf("[Task][List Availability Domains] failed to execute query, account %s, error: %s", t.Account, err)
		return generateError(err)
	}

	return generateResult(&AvailabilityDomainsResponse{
		AvailabilityDomains: response.Items,
	})
}

func (t *Task) ListShapes() *pb.Result {
	account, ok := accounts[t.Account]
	if !ok {
		return &pb.Result{
			Error: Error_AccountNotFound,
		}
	}

	tenancy, err := account.TenancyOCID()
	if err != nil {
		clog.Errorf("[Task][List Shapes] failed to get tenancy, account %s, error: %s", t.Account, err)
		return generateError(err)
	}

	var request ShapesRequest
	err = t.Decode(&request)
	if err != nil {
		clog.Errorf("[Task][List Shapes] failed to decode request, account %s, error: %s", t.Account, err)
		return &pb.Result{
			Error: Error_Decode,
		}
	}

	var ad *string
	if request.AvailabilityDomain != "" {
		ad = common.String(request.AvailabilityDomain)
	}

	response, err := account.compute.ListShapes(t.ctx,
		core.ListShapesRequest{
			CompartmentId:      &tenancy,
			AvailabilityDomain: ad,
			RequestMetadata: common.RequestMetadata{
				RetryPolicy: defaultPolicy,
			},
		})

	if err != nil {
		clog.Errorf("[Task][List Shapes] failed to execute query, account %s, error: %s", t.Account, err)
		return generateError(err)
	}
	return generateResult(ShapesResponse{
		Shapes: response.Items,
	})
}

func (t *Task) ListImages() *pb.Result {
	account, ok := accounts[t.Account]
	if !ok {
		return &pb.Result{
			Error: Error_AccountNotFound,
		}
	}

	tenancy, err := account.TenancyOCID()
	if err != nil {
		clog.Errorf("[Task][List Images] failed to get tenancy, account %s, error: %s", t.Account, err)
		return generateError(err)
	}

	var request ImagesRequest
	err = t.Decode(&request)
	if err != nil {
		clog.Errorf("[Task][List Images] failed to decode request, account %s, error: %s", t.Account, err)
		return &pb.Result{
			Error: Error_Decode,
		}
	}

	var os, shape *string
	if request.OS != "" {
		os = common.String(request.OS)
	}

	if request.Shape != "" {
		shape = common.String(request.Shape)
	}

	response, err := account.compute.ListImages(t.ctx,
		core.ListImagesRequest{
			CompartmentId:   &tenancy,
			OperatingSystem: os,
			Shape:           shape,
			RequestMetadata: common.RequestMetadata{
				RetryPolicy: defaultPolicy,
			},
		})

	if err != nil {
		clog.Errorf("[Task][List Images] failed to execute query, account %s, error: %s", t.Account, err)
		return generateError(err)
	}
	return generateResult(ImagesResponse{
		Images: response.Items,
	})
}

func (t *Task) ListInstances() *pb.Result {
	account, ok := accounts[t.Account]
	if !ok {
		return &pb.Result{
			Error: Error_AccountNotFound,
		}
	}

	tenancy, err := account.TenancyOCID()
	if err != nil {
		clog.Errorf("[Task][List Instances] failed to get tenancy, account %s, error: %s", t.Account, err)
		return generateError(err)
	}

	response, err := account.compute.ListInstances(t.ctx,
		core.ListInstancesRequest{
			CompartmentId: &tenancy,
			RequestMetadata: common.RequestMetadata{
				RetryPolicy: defaultPolicy,
			},
		})
	if err != nil {
		clog.Errorf("[Task][List Instances] failed to execute query, account %s, error: %s", t.Account, err)
		return generateError(err)
	}

	return generateResult(InstancesResponse{
		Instances: response.Items,
	})
}

func (t *Task) CreateInstance() *pb.Result {
	account, ok := accounts[t.Account]
	if !ok {
		return &pb.Result{
			Error: Error_AccountNotFound,
		}
	}

	tenancy, err := account.TenancyOCID()
	if err != nil {
		clog.Errorf("[Task][Create Instance] failed to get tenancy, account %s, error: %s", t.Account, err)
		return generateError(err)
	}

	var request CreateInstanceRequest
	err = t.Decode(&request)
	if err != nil {
		clog.Errorf("[Task][Create Instance] failed to decode request, account %s, error: %s", t.Account, err)
		return &pb.Result{
			Error: Error_Decode,
		}
	}

	var subnet core.Subnet
	{
		var vcn core.Vcn
		{
			vcns, err := account.network.ListVcns(t.ctx,
				core.ListVcnsRequest{
					CompartmentId: &tenancy,
					RequestMetadata: common.RequestMetadata{
						RetryPolicy: defaultPolicy,
					},
				})
			if err != nil {
				clog.Errorf("[Task][List Vcns] failed to execute query, account %s, error: %s", t.Account, err)
				return generateError(err)
			}

			if len(vcns.Items) >= 1 {
				vcn = vcns.Items[0]

				if len(vcn.Ipv6CidrBlocks) < 1 {
					_, err = account.network.AddIpv6VcnCidr(t.ctx,
						core.AddIpv6VcnCidrRequest{
							VcnId: vcn.Id,
							AddVcnIpv6CidrDetails: core.AddVcnIpv6CidrDetails{
								IsOracleGuaAllocationEnabled: common.Bool(true),
							},
							RequestMetadata: common.RequestMetadata{
								RetryPolicy: defaultPolicy,
							},
						})
					if err != nil {
						clog.Errorf("[Task][Add Ipv6 Vcn Cidr] failed to execute query, account %s, error: %s", t.Account, err)
						return generateError(err)
					}
				}
			} else {
				response, err := account.network.CreateVcn(t.ctx,
					core.CreateVcnRequest{
						CreateVcnDetails: core.CreateVcnDetails{
							CompartmentId: &tenancy,
							CidrBlocks:    []string{"10.0.0.0/16"},
							IsIpv6Enabled: common.Bool(true),
						},
						RequestMetadata: common.RequestMetadata{
							RetryPolicy: defaultPolicy,
						},
					})
				if err != nil {
					clog.Errorf("[Task][Create Vcn] failed to execute query, account %s, error: %s", t.Account, err)
					return generateError(err)
				}

				vcn = response.Vcn
			}
		}

		{
			var gateway core.InternetGateway
			{
				gateways, err := account.network.ListInternetGateways(t.ctx,
					core.ListInternetGatewaysRequest{
						CompartmentId: &tenancy,
						RequestMetadata: common.RequestMetadata{
							RetryPolicy: defaultPolicy,
						},
					})
				if err != nil {
					clog.Errorf("[Task][List Internet Gateways] failed to execute query, account %s, error: %s", t.Account, err)
					return generateError(err)
				}

				if len(gateways.Items) >= 1 {
					gateway = gateways.Items[0]
				} else {
					response, err := account.network.CreateInternetGateway(t.ctx,
						core.CreateInternetGatewayRequest{
							CreateInternetGatewayDetails: core.CreateInternetGatewayDetails{
								CompartmentId: &tenancy,
								IsEnabled:     common.Bool(true),
								VcnId:         vcn.Id,
							},
							RequestMetadata: common.RequestMetadata{
								RetryPolicy: defaultPolicy,
							},
						})
					if err != nil {
						clog.Errorf("[Task][List Internet Gateways] failed to execute query, account %s, error: %s", t.Account, err)
						return generateError(err)
					}

					gateway = response.InternetGateway
				}
			}

			tables, err := account.network.ListRouteTables(t.ctx,
				core.ListRouteTablesRequest{
					CompartmentId: &tenancy,
					VcnId:         vcn.Id,
					RequestMetadata: common.RequestMetadata{
						RetryPolicy: defaultPolicy,
					},
				})
			if err != nil {
				clog.Errorf("[Task][List Vcns] failed to execute query, account %s, error: %s", t.Account, err)
				return generateError(err)
			}

			if len(tables.Items) >= 1 {
				_, err = account.network.UpdateRouteTable(t.ctx, core.UpdateRouteTableRequest{
					RtId: tables.Items[0].Id,
					UpdateRouteTableDetails: core.UpdateRouteTableDetails{
						RouteRules: []core.RouteRule{
							{
								NetworkEntityId: gateway.Id,
								Destination:     common.String("0.0.0.0/0"),
								DestinationType: core.RouteRuleDestinationTypeCidrBlock,
								RouteType:       core.RouteRuleRouteTypeStatic,
							},
							{
								NetworkEntityId: gateway.Id,
								Destination:     common.String("::/0"),
								DestinationType: core.RouteRuleDestinationTypeCidrBlock,
								RouteType:       core.RouteRuleRouteTypeStatic,
							},
						},
					},
				})
				if err != nil {
					clog.Errorf("[Task][Update RouteTable] failed to execute query, account %s, error: %s", t.Account, err)
					return generateError(err)
				}
			} else {
				_, err = account.network.CreateRouteTable(t.ctx,
					core.CreateRouteTableRequest{
						CreateRouteTableDetails: core.CreateRouteTableDetails{
							CompartmentId: &tenancy,
							VcnId:         vcn.Id,
							RouteRules: []core.RouteRule{
								{
									NetworkEntityId: gateway.Id,
									Destination:     common.String("0.0.0.0/0"),
									DestinationType: core.RouteRuleDestinationTypeCidrBlock,
									RouteType:       core.RouteRuleRouteTypeStatic,
								},
								{
									NetworkEntityId: gateway.Id,
									Destination:     common.String("::/0"),
									DestinationType: core.RouteRuleDestinationTypeCidrBlock,
									RouteType:       core.RouteRuleRouteTypeStatic,
								},
							},
						},

						RequestMetadata: common.RequestMetadata{
							RetryPolicy: defaultPolicy,
						},
					})
				if err != nil {
					clog.Errorf("[Task][Create RouteTable] failed to execute query, account %s, error: %s", t.Account, err)
					return generateError(err)
				}
			}
		}

		subnets, err := account.network.ListSubnets(t.ctx,
			core.ListSubnetsRequest{
				CompartmentId: &tenancy,
				VcnId:         vcn.Id,
				RequestMetadata: common.RequestMetadata{
					RetryPolicy: defaultPolicy,
				},
			})
		if err != nil {
			clog.Errorf("[Task][List Subnets] failed to execute query, account %s, error: %s", t.Account, err)
			return generateError(err)
		}

		var ipv6CidrBlock *string
		if len(vcn.Ipv6CidrBlocks) >= 1 {
			pool := iplib.Net6FromStr(vcn.Ipv6CidrBlocks[0])
			blocks, err := pool.Subnet(64, 0)
			if err != nil {
				clog.Errorf("[Task][Generate IPv6 Subnet] failed to execute query, account %s, error: %s", t.Account, err)
				return generateError(err)
			}

			if len(blocks) >= 1 {
				ipv6CidrBlock = common.String(blocks[0].String())
			}
		}

		if len(subnets.Items) >= 1 {
			subnet = subnets.Items[0]

			if len(subnet.Ipv6CidrBlocks) == 0 || subnet.Ipv6CidrBlock == nil || *subnet.Ipv6CidrBlock == "" {
				_, err = account.network.UpdateSubnet(t.ctx,
					core.UpdateSubnetRequest{
						SubnetId: subnet.Id,
						UpdateSubnetDetails: core.UpdateSubnetDetails{
							Ipv6CidrBlock: ipv6CidrBlock,
						},
						RequestMetadata: common.RequestMetadata{
							RetryPolicy: defaultPolicy,
						},
					})
				if err != nil {
					clog.Errorf("[Task][Update Subnet] failed to execute query, account %s, error: %s", t.Account, err)
					return generateError(err)
				}
			}
		} else {
			response, err := account.network.CreateSubnet(t.ctx,
				core.CreateSubnetRequest{
					CreateSubnetDetails: core.CreateSubnetDetails{
						CompartmentId: &tenancy,
						VcnId:         vcn.Id,
						CidrBlock:     common.String("10.0.0.0/16"),
						Ipv6CidrBlock: ipv6CidrBlock,
					},
					RequestMetadata: common.RequestMetadata{
						RetryPolicy: defaultPolicy,
					},
				})
			if err != nil {
				clog.Errorf("[Task][List Subnets] failed to execute query, account %s, error: %s", t.Account, err)
				return generateError(err)
			}

			subnet = response.Subnet
		}
	}

	if len(subnet.SecurityListIds) >= 1 {
		_, err = account.network.UpdateSecurityList(t.ctx,
			core.UpdateSecurityListRequest{
				SecurityListId: &subnet.SecurityListIds[0],
				UpdateSecurityListDetails: core.UpdateSecurityListDetails{
					IngressSecurityRules: []core.IngressSecurityRule{
						{
							Protocol:   common.String("all"),
							Source:     common.String("0.0.0.0/0"),
							SourceType: core.IngressSecurityRuleSourceTypeCidrBlock,
						},
						{
							Protocol:   common.String("all"),
							Source:     common.String("::/0"),
							SourceType: core.IngressSecurityRuleSourceTypeCidrBlock,
						},
					},
					EgressSecurityRules: []core.EgressSecurityRule{
						{
							Protocol:        common.String("all"),
							Destination:     common.String("0.0.0.0/0"),
							DestinationType: core.EgressSecurityRuleDestinationTypeCidrBlock,
						},
						{
							Protocol:        common.String("all"),
							Destination:     common.String("::/0"),
							DestinationType: core.EgressSecurityRuleDestinationTypeCidrBlock,
						},
					},
				},
			})
		if err != nil {
			clog.Errorf("[Task][Update Security List] failed to execute query, account %s, error: %s", t.Account, err)
			return generateError(err)
		}
	}

	var sourceDetails core.InstanceSourceDetails
	switch {
	case request.BootVolumeId != "":
		sourceDetails = core.InstanceSourceViaBootVolumeDetails{
			BootVolumeId: &request.BootVolumeId,
		}
	case request.Image != "":
		if request.BootVolumeSize < 50 {
			request.BootVolumeSize = 50
		}

		if request.BootVolumeVPU < 10 {
			request.BootVolumeVPU = 50
		}

		sourceDetails = core.InstanceSourceViaImageDetails{
			ImageId:             &request.Image,
			BootVolumeSizeInGBs: &request.BootVolumeSize,
			BootVolumeVpusPerGB: &request.BootVolumeVPU,
		}
	default:
		return &pb.Result{
			Error: Error_MissingParams,
		}
	}

	var shapeConfig *core.LaunchInstanceShapeConfigDetails
	if request.ShapeConfig.OCPU > 0 && request.ShapeConfig.Memory > 0 {
		shapeConfig = &core.LaunchInstanceShapeConfigDetails{
			Ocpus:       common.Float32(float32(request.ShapeConfig.OCPU)),
			MemoryInGBs: common.Float32(float32(request.ShapeConfig.Memory)),
		}
	}

	var result GetInstanceResponse
	{
		response, err := account.compute.LaunchInstance(t.ctx,
			core.LaunchInstanceRequest{
				LaunchInstanceDetails: core.LaunchInstanceDetails{
					CompartmentId:      &tenancy,
					DisplayName:        &request.DisplayName,
					AvailabilityDomain: &request.AvailabilityDomain,
					Shape:              &request.Shape,
					ShapeConfig:        shapeConfig,
					SourceDetails:      sourceDetails,
					CreateVnicDetails: &core.CreateVnicDetails{
						AssignPublicIp: &request.AssignPublicIP,
						SubnetId:       subnet.Id,
					},
					IsPvEncryptionInTransitEnabled: common.Bool(true),
					Metadata: map[string]string{
						"ssh_authorized_keys": request.PublicKey,
					},
				},
				RequestMetadata: common.RequestMetadata{
					RetryPolicy: policy,
				},
			})
		if err != nil {
			clog.Errorf("[Task][Launch Instance] failed to execute query, account %s, error: %s", t.Account, err)
			return generateError(err)
		}

		result.Instance = response.Instance
	}

	{
		vnics, err := account.compute.ListVnicAttachments(t.ctx,
			core.ListVnicAttachmentsRequest{
				CompartmentId: &tenancy,
				InstanceId:    result.Instance.Id,
				RequestMetadata: common.RequestMetadata{
					RetryPolicy: defaultPolicy,
				},
			})
		if err != nil {
			clog.Errorf("[Task][List Vnic Attachments] failed to execute query, account %s, error: %s", t.Account, err)
			return generateError(err)
		}

		for _, vnic := range vnics.Items {
			privateIPs, err := account.network.ListPrivateIps(t.ctx,
				core.ListPrivateIpsRequest{
					VnicId: vnic.VnicId,
					RequestMetadata: common.RequestMetadata{
						RetryPolicy: defaultPolicy,
					},
				})
			if err != nil {
				clog.Errorf("[Task][List Private IPs] failed to execute query, account %s, error: %s", t.Account, err)
				return generateError(err)
			}

			for _, privateIP := range privateIPs.Items {
				response, err := account.network.GetPublicIpByPrivateIpId(t.ctx,
					core.GetPublicIpByPrivateIpIdRequest{
						GetPublicIpByPrivateIpIdDetails: core.GetPublicIpByPrivateIpIdDetails{
							PrivateIpId: privateIP.Id,
						},
						RequestMetadata: common.RequestMetadata{
							RetryPolicy: defaultPolicy,
						},
					})
				if response.HTTPResponse().StatusCode != 404 && err != nil {
					clog.Errorf("[Task][Get Public IP] failed to execute query, account %s, error: %s", t.Account, err)
					return generateError(err)
				}

				result.IPv4 = append(result.IPv4, IPv4{
					Private: privateIP,
					Public:  response.PublicIp,
				})
			}

			response, err := account.network.ListIpv6s(t.ctx,
				core.ListIpv6sRequest{
					VnicId: vnic.VnicId,
					RequestMetadata: common.RequestMetadata{
						RetryPolicy: defaultPolicy,
					},
				})
			if err != nil {
				clog.Errorf("[Task][List IPv6s] failed to execute query, account %s, error: %s", t.Account, err)
				return generateError(err)
			}

			result.IPv6 = response.Items
		}
	}

	{
		volumes, err := account.compute.ListBootVolumeAttachments(t.ctx,
			core.ListBootVolumeAttachmentsRequest{
				CompartmentId:      &tenancy,
				AvailabilityDomain: result.Instance.AvailabilityDomain,
				InstanceId:         result.Instance.Id,
				RequestMetadata: common.RequestMetadata{
					RetryPolicy: defaultPolicy,
				},
			},
		)
		if err != nil {
			clog.Errorf("[Task][List Boot Volume Attachments] failed to execute query, account %s, error: %s", t.Account, err)
			return generateError(err)
		}

		if len(volumes.Items) >= 1 {
			volume, err := account.storage.GetBootVolume(t.ctx,
				core.GetBootVolumeRequest{
					BootVolumeId: volumes.Items[0].BootVolumeId,
				})
			if err != nil {
				clog.Errorf("[Task][Get Boot Volume] failed to execute query, account %s, error: %s", t.Account, err)
				return generateError(err)
			}

			result.BootVolume = volume.BootVolume

			image, err := account.compute.GetImage(t.ctx,
				core.GetImageRequest{
					ImageId: volume.BootVolume.ImageId,
					RequestMetadata: common.RequestMetadata{
						RetryPolicy: defaultPolicy,
					},
				})
			if err != nil {
				clog.Errorf("[Task][Get Image] failed to execute query, account %s, error: %s", t.Account, err)
				return generateError(err)
			}
			result.Image = image.Image
		}
	}

	{
		volumes, err := account.compute.ListVolumeAttachments(t.ctx,
			core.ListVolumeAttachmentsRequest{
				CompartmentId: &tenancy,
				InstanceId:    result.Instance.Id,
				RequestMetadata: common.RequestMetadata{
					RetryPolicy: defaultPolicy,
				},
			},
		)
		if err != nil {
			clog.Errorf("[Task][List Volume Attachments] failed to execute query, account %s, error: %s", t.Account, err)
			return generateError(err)
		}

		for _, volume := range volumes.Items {
			volume, err := account.storage.GetVolume(t.ctx,
				core.GetVolumeRequest{
					VolumeId: volume.GetVolumeId(),
				})
			if err != nil {
				clog.Errorf("[Task][Get Volume] failed to execute query, account %s, error: %s", t.Account, err)
				return generateError(err)
			}

			result.Volumes = append(result.Volumes, volume.Volume)
		}
	}

	return generateResult(result)
}

func (t *Task) GetInstance() *pb.Result {
	account, ok := accounts[t.Account]
	if !ok {
		return &pb.Result{
			Error: Error_AccountNotFound,
		}
	}

	tenancy, err := account.TenancyOCID()
	if err != nil {
		clog.Errorf("[Task][Get Instance] failed to get tenancy, account %s, error: %s", t.Account, err)
		return generateError(err)
	}

	var request InstanceRequest
	err = t.Decode(&request)
	if err != nil {
		clog.Errorf("[Task][Get Instance] failed to decode request, account %s, error: %s", t.Account, err)
		return &pb.Result{
			Error: Error_Decode,
		}
	}

	var result GetInstanceResponse
	{
		response, err := account.compute.GetInstance(t.ctx,
			core.GetInstanceRequest{
				InstanceId: &request.InstanceId,
				RequestMetadata: common.RequestMetadata{
					RetryPolicy: defaultPolicy,
				},
			})
		if err != nil {
			clog.Errorf("[Task][Get Instance] failed to execute query, account %s, error: %s", t.Account, err)
			return generateError(err)
		}

		result.Instance = response.Instance
	}

	{
		vnics, err := account.compute.ListVnicAttachments(t.ctx,
			core.ListVnicAttachmentsRequest{
				CompartmentId: &tenancy,
				InstanceId:    &request.InstanceId,
				RequestMetadata: common.RequestMetadata{
					RetryPolicy: defaultPolicy,
				},
			})
		if err != nil {
			clog.Errorf("[Task][List Vnic Attachments] failed to execute query, account %s, error: %s", t.Account, err)
			return generateError(err)
		}

		for _, vnic := range vnics.Items {
			privateIPs, err := account.network.ListPrivateIps(t.ctx,
				core.ListPrivateIpsRequest{
					VnicId: vnic.VnicId,
					RequestMetadata: common.RequestMetadata{
						RetryPolicy: defaultPolicy,
					},
				})
			if err != nil {
				clog.Errorf("[Task][List Private IPs] failed to execute query, account %s, error: %s", t.Account, err)
				return generateError(err)
			}

			for _, privateIP := range privateIPs.Items {
				response, err := account.network.GetPublicIpByPrivateIpId(t.ctx,
					core.GetPublicIpByPrivateIpIdRequest{
						GetPublicIpByPrivateIpIdDetails: core.GetPublicIpByPrivateIpIdDetails{
							PrivateIpId: privateIP.Id,
						},
						RequestMetadata: common.RequestMetadata{
							RetryPolicy: defaultPolicy,
						},
					})
				if response.HTTPResponse().StatusCode != 404 && err != nil {
					clog.Errorf("[Task][Get Public IP] failed to execute query, account %s, error: %s", t.Account, err)
					return generateError(err)
				}

				result.IPv4 = append(result.IPv4, IPv4{
					Private: privateIP,
					Public:  response.PublicIp,
				})
			}

			response, err := account.network.ListIpv6s(t.ctx,
				core.ListIpv6sRequest{
					VnicId: vnic.VnicId,
					RequestMetadata: common.RequestMetadata{
						RetryPolicy: defaultPolicy,
					},
				})
			if err != nil {
				clog.Errorf("[Task][List IPv6s] failed to execute query, account %s, error: %s", t.Account, err)
				return generateError(err)
			}

			result.IPv6 = response.Items
		}
	}

	{
		volumes, err := account.compute.ListBootVolumeAttachments(t.ctx,
			core.ListBootVolumeAttachmentsRequest{
				CompartmentId:      &tenancy,
				AvailabilityDomain: result.Instance.AvailabilityDomain,
				InstanceId:         result.Instance.Id,
				RequestMetadata: common.RequestMetadata{
					RetryPolicy: defaultPolicy,
				},
			},
		)
		if err != nil {
			clog.Errorf("[Task][List Boot Volume Attachments] failed to execute query, account %s, error: %s", t.Account, err)
			return generateError(err)
		}

		if len(volumes.Items) >= 1 {
			volume, err := account.storage.GetBootVolume(t.ctx,
				core.GetBootVolumeRequest{
					BootVolumeId: volumes.Items[0].BootVolumeId,
				})
			if err != nil {
				clog.Errorf("[Task][Get Boot Volume] failed to execute query, account %s, error: %s", t.Account, err)
				return generateError(err)
			}

			result.BootVolume = volume.BootVolume

			image, err := account.compute.GetImage(t.ctx,
				core.GetImageRequest{
					ImageId: volume.BootVolume.ImageId,
					RequestMetadata: common.RequestMetadata{
						RetryPolicy: defaultPolicy,
					},
				})
			if err != nil {
				clog.Errorf("[Task][Get Image] failed to execute query, account %s, error: %s", t.Account, err)
				return generateError(err)
			}
			result.Image = image.Image
		}
	}

	{
		volumes, err := account.compute.ListVolumeAttachments(t.ctx,
			core.ListVolumeAttachmentsRequest{
				CompartmentId: &tenancy,
				InstanceId:    result.Instance.Id,
				RequestMetadata: common.RequestMetadata{
					RetryPolicy: defaultPolicy,
				},
			},
		)
		if err != nil {
			clog.Errorf("[Task][List Volume Attachments] failed to execute query, account %s, error: %s", t.Account, err)
			return generateError(err)
		}

		for _, volume := range volumes.Items {
			volume, err := account.storage.GetVolume(t.ctx,
				core.GetVolumeRequest{
					VolumeId: volume.GetVolumeId(),
				})
			if err != nil {
				clog.Errorf("[Task][Get Volume] failed to execute query, account %s, error: %s", t.Account, err)
				return generateError(err)
			}

			result.Volumes = append(result.Volumes, volume.Volume)
		}
	}

	return generateResult(result)
}

func (t *Task) UpdateInstance() *pb.Result {
	account, ok := accounts[t.Account]
	if !ok {
		return &pb.Result{
			Error: Error_AccountNotFound,
		}
	}

	var request UpdateInstanceRequest
	err := t.Decode(&request)
	if err != nil {
		clog.Errorf("[Task][Update Instance] failed to decode request, account %s, error: %s", t.Account, err)
		return &pb.Result{
			Error: Error_Decode,
		}
	}

	_, err = account.compute.UpdateInstance(t.ctx,
		core.UpdateInstanceRequest{
			InstanceId: &request.InstanceId,
			UpdateInstanceDetails: core.UpdateInstanceDetails{
				DisplayName: &request.DisplayName,
			},
			RequestMetadata: common.RequestMetadata{
				RetryPolicy: defaultPolicy,
			},
		})
	if err != nil {
		clog.Errorf("[Task][Update Instance] failed to execute query, account %s, error: %s", t.Account, err)
		return generateError(err)
	}

	return &pb.Result{Error: Error_NoError}
}

func (t *Task) InstanceAction() *pb.Result {
	account, ok := accounts[t.Account]
	if !ok {
		return &pb.Result{
			Error: Error_AccountNotFound,
		}
	}

	var request InstanceActionRequest
	err := t.Decode(&request)
	if err != nil {
		clog.Errorf("[Task][Instance Action] failed to decode request, account %s, error: %s", t.Account, err)
		return &pb.Result{
			Error: Error_Decode,
		}
	}

	_, err = account.compute.InstanceAction(t.ctx,
		core.InstanceActionRequest{
			InstanceId: &request.InstanceId,
			Action:     request.Action,
			RequestMetadata: common.RequestMetadata{
				RetryPolicy: policy,
			},
		})
	if err != nil {
		clog.Errorf("[Task][Instance Action] failed to execute query, account %s, error: %s", t.Account, err)
		return generateError(err)
	}

	return &pb.Result{Error: Error_NoError}
}

func (t *Task) TerminateInstance() *pb.Result {
	account, ok := accounts[t.Account]
	if !ok {
		return &pb.Result{
			Error: Error_AccountNotFound,
		}
	}

	var request TerminateInstanceRequest
	err := t.Decode(&request)
	if err != nil {
		clog.Errorf("[Task][Terminate Instance] failed to decode request, account %s, error: %s", t.Account, err)
		return &pb.Result{
			Error: Error_Decode,
		}
	}

	_, err = account.compute.TerminateInstance(t.ctx,
		core.TerminateInstanceRequest{
			InstanceId:         &request.InstanceId,
			PreserveBootVolume: &request.PreserveBootVolume,
			RequestMetadata: common.RequestMetadata{
				RetryPolicy: defaultPolicy,
			},
		})
	if err != nil {
		clog.Errorf("[Task][Terminate Instance] failed to execute query, account %s, error: %s", t.Account, err)
		return generateError(err)
	}

	return &pb.Result{Error: Error_NoError}
}

func (t *Task) InstanceConsole() *pb.Result {
	account, ok := accounts[t.Account]
	if !ok {
		return &pb.Result{
			Error: Error_AccountNotFound,
		}
	}

	tenancy, err := account.TenancyOCID()
	if err != nil {
		clog.Errorf("[Task][Instance Console] failed to get tenancy, account %s, error: %s", t.Account, err)
		return generateError(err)
	}

	var request InstanceConsoleRequest
	err = t.Decode(&request)
	if err != nil {
		clog.Errorf("[Task][Instance Console] failed to decode request, account %s, error: %s", t.Account, err)
		return &pb.Result{
			Error: Error_Decode,
		}
	}

	var result InstanceConsoleResponse
	{
		response, err := account.compute.GetInstance(t.ctx,
			core.GetInstanceRequest{
				InstanceId: &request.InstanceId,
				RequestMetadata: common.RequestMetadata{
					RetryPolicy: defaultPolicy,
				},
			})
		if err != nil {
			clog.Errorf("[Task][Get Instance] failed to execute query, account %s, error: %s", t.Account, err)
			return generateError(err)
		}

		result.Region = *response.Region
	}

	{
		consoles, err := account.compute.ListInstanceConsoleConnections(t.ctx,
			core.ListInstanceConsoleConnectionsRequest{
				CompartmentId: &tenancy,
				InstanceId:    &request.InstanceId,
				RequestMetadata: common.RequestMetadata{
					RetryPolicy: defaultPolicy,
				},
			})
		if err != nil {
			clog.Errorf("[Task][List Instance Console Connection] failed to execute query, account %s, error: %s", t.Account, err)
			return generateError(err)
		}

		for _, console := range consoles.Items {
			_, err = account.compute.DeleteInstanceConsoleConnection(t.ctx,
				core.DeleteInstanceConsoleConnectionRequest{
					InstanceConsoleConnectionId: console.Id,
					RequestMetadata: common.RequestMetadata{
						RetryPolicy: alwaysPolicy,
					},
				})
			if err != nil {
				clog.Errorf("[Task][Delete Instance Console Connection] failed to execute query, account %s, error: %s", t.Account, err)
				return generateError(err)
			}
		}
	}

	{
		response, err := account.compute.CreateInstanceConsoleConnection(t.ctx,
			core.CreateInstanceConsoleConnectionRequest{
				CreateInstanceConsoleConnectionDetails: core.CreateInstanceConsoleConnectionDetails{
					InstanceId: &request.InstanceId,
					PublicKey:  &request.PublicKey,
				},
				RequestMetadata: common.RequestMetadata{
					RetryPolicy: alwaysPolicy,
				},
			})
		if err != nil {
			clog.Errorf("[Task][Create Instance Console Connection] failed to execute query, account %s, error: %s", t.Account, err)
			return generateError(err)
		}

		result.Console = response.InstanceConsoleConnection
	}
	return generateResult(result)
}

func (t *Task) ChangeIPv4() *pb.Result {
	account, ok := accounts[t.Account]
	if !ok {
		return &pb.Result{
			Error: Error_AccountNotFound,
		}
	}

	tenancy, err := account.TenancyOCID()
	if err != nil {
		clog.Errorf("[Task][Change IPv4] failed to get tenancy, account %s, error: %s", t.Account, err)
		return generateError(err)
	}

	var request IPRequest
	err = t.Decode(&request)
	if err != nil {
		clog.Errorf("[Task][Change IPv4] failed to decode request, account %s, error: %s", t.Account, err)
		return &pb.Result{
			Error: Error_Decode,
		}
	}

	var lifetime core.CreatePublicIpDetailsLifetimeEnum = core.CreatePublicIpDetailsLifetimeEphemeral

	publicIp, err := account.network.GetPublicIpByPrivateIpId(t.ctx,
		core.GetPublicIpByPrivateIpIdRequest{
			GetPublicIpByPrivateIpIdDetails: core.GetPublicIpByPrivateIpIdDetails{
				PrivateIpId: &request.PrivateId,
			},
			RequestMetadata: common.RequestMetadata{
				RetryPolicy: defaultPolicy,
			},
		})
	if publicIp.HTTPResponse().StatusCode != 404 && err != nil {
		clog.Errorf("[Task][Get Public IP] failed to execute query, account %s, error: %s", t.Account, err)
		return generateError(err)
	}

	if publicIp.HTTPResponse().StatusCode != 404 {
		_, err = account.network.DeletePublicIp(t.ctx,
			core.DeletePublicIpRequest{
				PublicIpId: publicIp.Id,
				RequestMetadata: common.RequestMetadata{
					RetryPolicy: defaultPolicy,
				},
			})
		if err != nil {
			clog.Errorf("[Task][Delete Public IP] failed to execute query, account %s, error: %s", t.Account, err)
			return generateError(err)
		}

		lifetime = core.CreatePublicIpDetailsLifetimeEnum(publicIp.Lifetime)
	}

	response, err := account.network.CreatePublicIp(t.ctx,
		core.CreatePublicIpRequest{
			CreatePublicIpDetails: core.CreatePublicIpDetails{
				CompartmentId: &tenancy,
				PrivateIpId:   &request.PrivateId,
				Lifetime:      lifetime,
			},
			RequestMetadata: common.RequestMetadata{
				RetryPolicy: defaultPolicy,
			},
		})
	if err != nil {
		clog.Errorf("[Task][Create Public IP] failed to execute query, account %s, error: %s", t.Account, err)
		return generateError(err)
	}

	return generateResult(ChangeIPv4Response{
		Public: response.PublicIp,
	})
}

func (t *Task) ChangeIPv6() *pb.Result {
	account, ok := accounts[t.Account]
	if !ok {
		return &pb.Result{
			Error: Error_AccountNotFound,
		}
	}

	var request IPRequest
	err := t.Decode(&request)
	if err != nil {
		clog.Errorf("[Task][Change IPv6] failed to decode request, account %s, error: %s", t.Account, err)
		return &pb.Result{
			Error: Error_Decode,
		}
	}

	ip, err := account.network.GetIpv6(t.ctx,
		core.GetIpv6Request{
			Ipv6Id: &request.PublicId,
			RequestMetadata: common.RequestMetadata{
				RetryPolicy: defaultPolicy,
			},
		})
	if err != nil {
		clog.Errorf("[Task][Get IPv6] failed to execute query, account %s, error: %s", t.Account, err)
		return generateError(err)
	}

	_, err = account.network.DeleteIpv6(t.ctx,
		core.DeleteIpv6Request{
			Ipv6Id: ip.Id,
			RequestMetadata: common.RequestMetadata{
				RetryPolicy: defaultPolicy,
			},
		})
	if err != nil {
		clog.Errorf("[Task][Delete IPv6] failed to execute query, account %s, error: %s", t.Account, err)
		return generateError(err)
	}

	response, err := account.network.CreateIpv6(t.ctx,
		core.CreateIpv6Request{
			CreateIpv6Details: core.CreateIpv6Details{
				VnicId: ip.VnicId,
			},
			RequestMetadata: common.RequestMetadata{
				RetryPolicy: defaultPolicy,
			},
		})
	if err != nil {
		clog.Errorf("[Task][Create IPv6] failed to execute query, account %s, error: %s", t.Account, err)
		return generateError(err)
	}

	return generateResult(ChangeIPv6Response{
		Public: response.Ipv6,
	})
}

func (t *Task) ListBootVolumes() *pb.Result {
	account, ok := accounts[t.Account]
	if !ok {
		return &pb.Result{
			Error: Error_AccountNotFound,
		}
	}

	tenancy, err := account.TenancyOCID()
	if err != nil {
		clog.Errorf("[Task][List Boot Volumes] failed to get tenancy, account %s, error: %s", t.Account, err)
		return generateError(err)
	}

	response, err := account.storage.ListBootVolumes(t.ctx,
		core.ListBootVolumesRequest{
			CompartmentId: &tenancy,
		})
	if err != nil {
		clog.Errorf("[Task][List Boot Volumes] failed to execute query, account %s, error: %s", t.Account, err)
		return generateError(err)
	}

	return generateResult(ListBootVolumesResponse{
		BootVolumes: response.Items,
	})
}
func (t *Task) Unknown() *pb.Result {
	return &pb.Result{
		Error: Error_UnknownFunction,
	}
}
