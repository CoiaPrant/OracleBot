package types

import "github.com/oracle/oci-go-sdk/v65/core"

type InstancesResponse struct {
	Instances []core.Instance
}

type InstanceRequest struct {
	InstanceId string
}

type CreateInstanceRequest struct {
	DisplayName        string
	AvailabilityDomain string

	Shape       string
	ShapeConfig ShapeConfig

	Image, BootVolumeId string
	BootVolumeSize      int64
	BootVolumeVPU       int64

	AssignPublicIP bool
	PublicKey      string
}

type ShapeConfig struct {
	OCPU   int64
	Memory int64
}

type IPv4 struct {
	Private core.PrivateIp
	Public  core.PublicIp
}

type GetInstanceResponse struct {
	Instance core.Instance

	IPv4 []IPv4
	IPv6 []core.Ipv6

	BootVolume core.BootVolume
	Image      core.Image
	Volumes    []core.Volume
}

type UpdateInstanceRequest struct {
	InstanceId  string
	DisplayName string
}

type InstanceActionRequest struct {
	InstanceId string
	Action     core.InstanceActionActionEnum
}

type TerminateInstanceRequest struct {
	InstanceId         string
	PreserveBootVolume bool
}

type InstanceConsoleRequest struct {
	InstanceId string
	PublicKey  string
}

type InstanceConsoleResponse struct {
	Region  string
	Console core.InstanceConsoleConnection
}
