package types

import "github.com/oracle/oci-go-sdk/v65/core"

type ListBootVolumesResponse struct {
	BootVolumes []core.BootVolume
}
