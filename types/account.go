package types

import (
	"github.com/oracle/oci-go-sdk/v65/core"
	"github.com/oracle/oci-go-sdk/v65/identity"
)

type AvailabilityDomainsResponse struct {
	AvailabilityDomains []identity.AvailabilityDomain
}

type ShapesRequest struct {
	AvailabilityDomain string
}

type ShapesResponse struct {
	Shapes []core.Shape
}

type ImagesRequest struct {
	Shape string
	OS    string
}

type ImagesResponse struct {
	Images []core.Image
}
