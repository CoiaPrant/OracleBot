package types

import "github.com/oracle/oci-go-sdk/v65/core"

type IPRequest struct {
	PrivateId string
	PublicId  string
}

type ChangeIPv4Response struct {
	Public core.PublicIp
}

type ChangeIPv6Response struct {
	Public core.Ipv6
}
