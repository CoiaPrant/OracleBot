package types

const (
	Task_Ping int64 = iota
	Task_Temporary
	Task_Enduring
	Task_CancelEnduring
)

const (
	// Account
	Function_ListAccount int64 = iota
	Function_ListAvailabilityDomains
	Function_ListShapes
	Function_ListImages

	// Instance
	Function_ListInstances
	Function_CreateInstance
	Function_GetInstance
	Function_UpdateInstance
	Function_InstanceAction
	Function_TerminateInstance
	Function_InstanceConsole

	// Network
	Function_ChangeIPv4
	Function_ChangeIPv6

	// Block Storages
	Function_ListBootVolumes
)

const (
	Error_NoError int64 = iota
	Error_UnknownFunction
	Error_DisabledFunction
	Error_Canceled
	Error_Encode
	Error_Decode
	Error_AccountNotFound
	Error_MissingParams
	Error_Other
)
