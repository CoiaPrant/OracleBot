# OracleBot

甲骨文云管理 Bot

## 一键脚本

```shell
wget -O oci.sh -N https://gitlab.com/CoiaPrant/OracleBot/-/raw/master/scripts/manage.sh && bash oci.sh
```

## 感谢

- [Misaka-blog](https://gitlab.com/Misaka-blog) 制作的管理脚本

## 友链

- [二刺螈日常](https://t.me/acgdaily) 沙雕日常频道
- [Coia's Blog & Daily](https://t.me/CoiaBlog) 技术类和一些可公开日常, 也许会有更新通知
- [Topicgram](https://t.me/TopicgramUpdates) 私信机器人, Livegram 的替代品, 使用 Topic 以获得原生的 Telegram 聊天体验 
